package bk.idea;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {

    public static void main(String[] args) {
        //The Array way

        //Neuen Array der Größe 10 erstellen, nun sollen aber 11 Elemente gespeichert werden.
        int[] arr = new int[10];

        //Zuerst neues Array mit korrekter größe erstellen
        int[] arr2 = new int[11];
        //Über den ersten Array iterieren
        for (int i = 0; i < arr.length; i++) {
            arr2[i] = arr[i]; // Jedes element aus arr an der stelle i in arr2 an der stelle i kopieren
        }

        //11.Element speichern
        arr2[10] = 5;

        //<=============================================================================================>

        //The List way

        //ArrayList erstellen. Dabei wird in den spitzen Klammern der Typ, welcher in die Liste geschrieben werden sol angegeben.
        ArrayList<String> list = new ArrayList<>();

        //Mit .add können beliebig viele Elemente hinzugefügt werden.
        list.add("Felix");
        list.add("Clara");
        list.add("Daniel");

        //Mit .get können Elemente aus der Liste geholt werden
        System.out.println(list.get(1));

        //Gibt die ganze liste aus
        System.out.println(list);

        //<=============================================================================================>

        //Maps - Abbildung von Werten auf andere Werte

        //Hashmap erstellen. In Spizen klammern wird der Typ für den Schlüssel und der typ des Wertes angegeben.
        HashMap<String, Integer> map = new HashMap<>();

        //Mit put kann ein neues Schlüssel-Wert-Paar eingefügt werden
        map.put("Hallo", 1);

        //Den Wert zu "Hallo" aus der map holen, um eins erhöhen und wieder dem schlüssel "Hallo" zuweisen
        map.put("Hallo",map.get("Hallo")+1);

        System.out.println(map);
    }

}
